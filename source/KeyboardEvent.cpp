#include "KeyboardEvent.hpp"
// -----------------------------------------------------------------------------

KeyboardEvent::
KeyboardEvent(Key key, bool autoRepeat): _key(key), _autoRepeat(autoRepeat)
{}

Key
KeyboardEvent::
key() const
{ return _key; }

bool
KeyboardEvent::
isAutoRepeat() const
{ return _autoRepeat; }