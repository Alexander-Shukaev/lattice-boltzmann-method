#ifndef FlagField_hpp
#define FlagField_hpp

#include "Field.hpp"

class FlagField: public Field {
public:
  FlagField(GLuint sampleCountX,
            GLuint sampleCountY,
            GLuint sampleCountZ);
};

#endif