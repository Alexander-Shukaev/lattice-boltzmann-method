#ifndef sgn_hpp
#define sgn_hpp

namespace Math {
template <typename T>
int
sgn(T x)
{ return (T(0) < x) - (x < T(0)); }
}

#endif