#ifndef Renderer_hpp
#define Renderer_hpp

class Renderer {
public:
  virtual
  ~Renderer() = 0;

  virtual void
  initialize() = 0;

  virtual void
  resize(int width, int height) = 0;

  virtual void
  render() = 0;
};

#endif
