#ifndef Button_hpp
#define Button_hpp

#include <Qt>

enum class Button: int  {
  left  = Qt::LeftButton,
  right = Qt::RightButton,
  none  = Qt::NoButton,
};

#endif