#ifndef DomainRenderer_hpp
#define DomainRenderer_hpp

#include "Matrix.hpp"

#include <GL/glew.h>

#include <oglplus/all.hpp>

class DomainRenderer {
public:
  DomainRenderer(GLuint cellCountX,
                 GLuint cellCountY,
                 GLuint cellCountZ);

  void
  render(Math::Matrix4x4f const& mvp, GLfloat r, GLfloat g, GLfloat b) const;

private:
  void
  initializeProgram();

  void
  initializeDomain();

  oglplus::Context _gl;

  oglplus::Program _program;

  oglplus::VertexArray _vertexArray;

  oglplus::Buffer _vertexBuffer;

  oglplus::Buffer _indexBuffer;

  GLuint _cellCountX;
  GLuint _cellCountY;
  GLuint _cellCountZ;
};

#endif