#include "Animator.hpp"

#include <QtOpenGL/QGLWidget>

#include <cassert>
// -----------------------------------------------------------------------------

Animator::
Animator(QGLWidget* glWidget): QObject(glWidget) {
  assert(glWidget);

  _timer.setSingleShot(false);

  connect(&_timer, SIGNAL(timeout()), glWidget, SLOT(update()));
}

void
Animator::
start(int milliseconds)
{ _timer.start(milliseconds); }

void
Animator::
stop()
{ _timer.stop(); }