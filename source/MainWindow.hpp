#ifndef MainWindow_hpp
#define MainWindow_hpp

#include <QtGui/QMainWindow>

class QCloseEvent;
class QMdiArea;
class QAction;
class QLineEdit;
class QSpinBox;
class QTooolBar;

class MainWindow: public QMainWindow {
  Q_OBJECT

public:
  explicit
  MainWindow();

private:
  void
  createActions();

  void
  createToolBars();

public:
  QMdiArea*
  mdiArea() const;

  QAction*
  runSimulationAction() const;

  QAction*
  resetSimulationAction() const;

  QAction*
  toggleSimulationModeAction() const;

public slots:
  void
  changeSimulationMode(int mode);

public:
  QAction*
  increaseDelayAction() const;

  QAction*
  reduceDelayAction() const;

public:
  int
  domainSizeX() const;

signals:
  void
  domainSizeXChanged(int sizeX);

public:
  int
  domainSizeY() const;

signals:
  void
  domainSizeYChanged(int sizeY);

public:
  int
  domainSizeZ() const;

signals:
  void
  domainSizeZChanged(int sizeZ);

private slots:
  void
  changeScenarioFilePath();

public:
  QString const&
  scenarioFilePath() const;

signals:
  void
  scenarioFilePathChanged(QString const& scenarioFilePath);

protected:
  void
  closeEvent(QCloseEvent* event);

private:
  QMdiArea* _mdiArea;

  QAction* _runSimulationAction;
  QAction* _resetSimulationAction;
  QAction* _toggleSimulationMode;
  QAction* _increaseDelayAction;
  QAction* _reduceDelayAction;

  QSpinBox* _domainSizeX;
  QSpinBox* _domainSizeY;
  QSpinBox* _domainSizeZ;

  QAction*   _chooseFileAction;
  QLineEdit* _scenarioFilePathLineEdit;

  QToolBar* _toolBar;
};

#endif
