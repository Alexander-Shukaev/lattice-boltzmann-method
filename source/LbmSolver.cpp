#include "LbmSolver.hpp"

namespace CL {
void
maxOneDGroupSize(
  Device const& device,
  NDRange&      global,
  NDRange&      local) {
  try {
    ::size_t size = 512;
    // device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
    ::size_t rest = global[0] % size;

    if (rest != 0)
      global = CL::NDRange(global[0] - rest + size);

    local = CL::NDRange(size);
  } catch (CL::Error const& exc) {
    FATAL("An error occured during OpenCL initialization at '%s': %i",
          exc.what(), exc.err());
  }
}
}

LbmSolver::
LbmSolver(): _swap(true) {}

LbmSolver::
LbmSolver(
  LbmSolver const& other) {}

LbmSolver::~LbmSolver() {}

LbmSolver const&
LbmSolver::
operator=(
  LbmSolver const& other) {
  return *this;
}

void
LbmSolver::
kernelSource(std::string const& sourceFileName) {
  _kernelSource = Source(sourceFileName.c_str());
}

void
LbmSolver::
routineSource(std::string const& sourceFileName) {
  _routineSource = Source(sourceFileName.c_str());
}

void
LbmSolver::
includeFiles(std::string const& includeFilePath) {
  _includeSourceList.clear();
  _includeSourceList.push_back(Source(includeFilePath.c_str()));
}

void
LbmSolver::
includeFiles(IncludeFilePathList const& includeFilePathList) {
  _includeSourceList.clear();

  for (int i = 0; i < includeFilePathList.size(); ++i)
    _includeSourceList.push_back(Source(includeFilePathList[i].c_str()));
}

void
LbmSolver::
initialize() {
  try {
    _sharedClInitialization->initialize();

    CL::Context const&    context = _sharedClInitialization->context();
    CL::DeviceList const& devices = _sharedClInitialization->devices();

    // Create a command queue and use the first device
    _queue = CL::CommandQueue(context, devices[0]);

    std::string sourceCode;

    sourceCode.append(_routineSource.get());

    for (int i = 0; i < _includeSourceList.size(); ++i)
      sourceCode.append(_includeSourceList[i].get());

    sourceCode.append(_kernelSource.get());

    //  PRINT("%s\n", _kernelSource.get());
    //  PRINT("%u\n", _kernelSource.length());
    //  PRINT("%s\n", _routineSource.get());
    //  PRINT("%u\n", _routineSource.length());

    // Make program of the source code in the context
    CL::Program::Sources sources(1,
                                 std::make_pair(sourceCode.c_str(),
                                                sourceCode.size() + 1));
    _program = CL::Program(context, sources);

    // Build program for these specific devices
    try {
      _program.build(devices);
    } catch (CL::Error const& exc) {
      PRINT("An error occured during OpenCL initialization at '%s': %i",
            exc.what(), exc.err());
      std::string buildLog = _program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(
        devices[0]);
      printf(buildLog.c_str());
      exit(1);
    }

    // Make kernel
    _fillFlagsKernel     = CL::Kernel(_program, "fillFlags");
    _doStreamingKernel   = CL::Kernel(_program, "doStreaming");
    _doCollisionKernel   = CL::Kernel(_program, "doCollision");
    _treatBoundaryKernel = CL::Kernel(_program, "treatBoundary");

    _configBuffer = CL::Buffer(
      context,
      CL_MEM_READ_ONLY,
      sizeof(Config));

    _fillFlagsKernel.setArg(0, _configBuffer);
    _doStreamingKernel.setArg(0, _configBuffer);
    _doCollisionKernel.setArg(0, _configBuffer);
    _treatBoundaryKernel.setArg(0, _configBuffer);

    _glObjects.clear();
    initLbmDef();
    setSize();
    setTau();
    setWallVelocity();
    updateConfig();
    setBuffers();
    updateFlags();
  } catch (CL::Error const& exc) {
    FATAL("An error occured during OpenCL initialization at '%s': %i",
          exc.what(), exc.err());
  }
}

void
LbmSolver::
initLbmDef() {
  for (int i = 0; i < Q; ++i) {
    _config.lbmDef.LATTICEVELOCITIES[i].s[0] = LATTICEVELOCITIES[i][0];
    _config.lbmDef.LATTICEVELOCITIES[i].s[1] = LATTICEVELOCITIES[i][1];
    _config.lbmDef.LATTICEVELOCITIES[i].s[2] = LATTICEVELOCITIES[i][2];
    _config.lbmDef.LATTICEWEIGHTS[i]         = LATTICEWEIGHTS[i];
  }
}

void
LbmSolver::
setSize() {
  ::size_t resolution = _sizeX * _sizeY * _sizeZ;

  PRINT("%u\n", _sizeX);
  PRINT("%u\n", _sizeY);
  PRINT("%u\n", _sizeZ);
  try {
    ::size_t flagFieldBufferSize = resolution * sizeof(cl_char);

    _flagFieldBuffer = CL::Buffer(_sharedClInitialization->context(),
                                  CL_MEM_READ_WRITE,
                                  flagFieldBufferSize,
                                  0);

    ::size_t streamFieldBufferSize = resolution * 19 * sizeof(cl_double);

    _streamFieldBuffer = CL::Buffer(_sharedClInitialization->context(),
                                    CL_MEM_READ_WRITE,
                                    streamFieldBufferSize,
                                    0);

    ::size_t collideFieldBufferSize = resolution * 19 * sizeof(cl_double);

    _collideFieldBuffer = CL::Buffer(_sharedClInitialization->context(),
                                     CL_MEM_READ_WRITE,
                                     collideFieldBufferSize,
                                     0);

    ::size_t densityBufferSize = resolution * 1 * sizeof(cl_float);

    _densityBuffer = CL::Buffer(_sharedClInitialization->context(),
                                CL_MEM_READ_WRITE,
                                densityBufferSize,
                                0);

    ::size_t velocityBufferSize = resolution * 4 * sizeof(cl_float);

    _velocityBuffer = CL::Buffer(_sharedClInitialization->context(),
                                 CL_MEM_READ_WRITE,
                                 velocityBufferSize,
                                 0);

    _fillFlagsKernel.setArg(1, _flagFieldBuffer);
    _fillFlagsKernel.setArg(2, _streamFieldBuffer);
    _fillFlagsKernel.setArg(3, _collideFieldBuffer);
    _fillFlagsKernel.setArg(4, _densityBuffer);
    _fillFlagsKernel.setArg(5, _velocityBuffer);

    _doStreamingKernel.setArg(1, _flagFieldBuffer);

    _doCollisionKernel.setArg(1, _flagFieldBuffer);
    _doCollisionKernel.setArg(3, _densityBuffer);
    _doCollisionKernel.setArg(4, _velocityBuffer);

    _treatBoundaryKernel.setArg(1, _flagFieldBuffer);

    _global = CL::NDRange(resolution);
    CL::maxOneDGroupSize(
      _sharedClInitialization->devices()[0],
      _global,
      _local);
    PRINT("Global: %u", _global[0]);
    PRINT("Local: %u", _local[0]);

    _config.sizeX       = _sizeX;
    _config.sizeY       = _sizeY;
    _config.sizeZ       = _sizeZ;
    _config.resolution = resolution;
  } catch (CL::Error const& exc) {
    FATAL("An error occured during OpenCL initialization at '%s': %i",
          exc.what(), exc.err());
  }
}

void
LbmSolver::
setTau() {
  _config.tau = _tau;
}

void
LbmSolver::
setWallVelocity() {
  _config.wallVelocity = _wallVeclocity;
}

void
LbmSolver::
setDensityBuffer(
  GLenum target,
  GLuint textureObject) {
  try {
    _densityImage = CL::Image3DGL(_sharedClInitialization->context(),
                                  CL_MEM_READ_WRITE,
                                  target,
                                  0,
                                  textureObject);
  } catch (CL::Error const& exc) {
    FATAL("An error occured during OpenCL initialization at '%s': %i",
          exc.what(), exc.err());
  }
}

void
LbmSolver::
updateDensityBuffer() {
  CL::size_t<3> origin;
  origin.push_back(0);
  origin.push_back(0);
  origin.push_back(0);
  CL::size_t<3> region;
  region.push_back(_config.sizeX);
  region.push_back(_config.sizeY);
  region.push_back(_config.sizeZ);

  _glObjects.clear();
  _glObjects.push_back(_densityImage);

#ifndef NDEBUG
  try {
#endif
  _queue.enqueueAcquireGLObjects(&_glObjects);
  _queue.enqueueCopyBufferToImage(_densityBuffer,
                                  _densityImage,
                                  0,
                                  origin,
                                  region);
  _queue.enqueueReleaseGLObjects(&_glObjects);
#ifndef NDEBUG
} catch (CL::Error const& exc) {
  FATAL("An error occured during OpenCL initialization at '%s': %i",
        exc.what(), exc.err());
}

#endif
}

void
LbmSolver::
setVelocityBuffer(
  GLenum target,
  GLuint textureObject) {
  try {
    _velocityImage = CL::Image3DGL(_sharedClInitialization->context(),
                                   CL_MEM_READ_WRITE,
                                   target,
                                   0,
                                   textureObject);
  } catch (CL::Error const& exc) {
    FATAL("An error occured during OpenCL initialization at '%s': %i",
          exc.what(), exc.err());
  }
}

void
LbmSolver::
updateVelocityBuffer() {
  CL::size_t<3> origin;
  origin.push_back(0);
  origin.push_back(0);
  origin.push_back(0);
  CL::size_t<3> region;
  region.push_back(_config.sizeX);
  region.push_back(_config.sizeY);
  region.push_back(_config.sizeZ);

  _glObjects.clear();
  _glObjects.push_back(_velocityImage);

#ifndef NDEBUG
  try {
#endif
  _queue.enqueueAcquireGLObjects(&_glObjects);
  _queue.enqueueCopyBufferToImage(_velocityBuffer,
                                  _velocityImage,
                                  0,
                                  origin,
                                  region);
  _queue.enqueueReleaseGLObjects(&_glObjects);
#ifndef NDEBUG
} catch (CL::Error const& exc) {
  FATAL("An error occured during OpenCL initialization at '%s': %i",
        exc.what(), exc.err());
}

#endif
}

void
LbmSolver::
setFlagBuffer(
  GLenum target,
  GLuint textureObject) {
  try {
    _flagImage = CL::Image3DGL(_sharedClInitialization->context(),
                               CL_MEM_READ_WRITE,
                               target,
                               0,
                               textureObject);
  } catch (CL::Error const& exc) {
    FATAL("An error occured during OpenCL initialization at '%s': %i",
          exc.what(), exc.err());
  }
}

void
LbmSolver::
updateFlagBuffer() {
  CL::size_t<3> origin;
  origin.push_back(0);
  origin.push_back(0);
  origin.push_back(0);
  CL::size_t<3> region;
  region.push_back(_config.sizeX);
  region.push_back(_config.sizeY);
  region.push_back(_config.sizeZ);

  _glObjects.clear();
  _glObjects.push_back(_flagImage);

#ifndef NDEBUG
  try {
#endif
  _queue.enqueueAcquireGLObjects(&_glObjects);
  _queue.enqueueCopyBufferToImage(_flagFieldBuffer,
                                  _flagImage,
                                  0,
                                  origin,
                                  region);
  _queue.enqueueReleaseGLObjects(&_glObjects);
#ifndef NDEBUG
} catch (CL::Error const& exc) {
  FATAL("An error occured during OpenCL initialization at '%s': %i",
        exc.what(), exc.err());
}

#endif
}

void
LbmSolver::
updateConfig() {
#ifndef NDEBUG
  try {
#endif
  _queue.enqueueWriteBuffer(_configBuffer, CL_TRUE, 0,
                            sizeof(Config), &_config);
#ifndef NDEBUG
} catch (CL::Error const& exc) {
  FATAL("An error occured during OpenCL initialization at '%s': %i",
        exc.what(), exc.err());
}

#endif
}

void
LbmSolver::
updateFlags() {
  _queue.enqueueNDRangeKernel(_fillFlagsKernel,
                              CL::NullRange,
                              _global,
                              _local);
}

void
LbmSolver::
run() {
#ifndef NDEBUG
  try {
#endif

  _queue.enqueueNDRangeKernel(_doStreamingKernel,
                              CL::NullRange,
                              _global,
                              _local);

  swapBuffers();
  setBuffers();
  _queue.enqueueNDRangeKernel(_doCollisionKernel,
                              CL::NullRange,
                              _global,
                              _local);
  _queue.finish();

  _queue.enqueueNDRangeKernel(_treatBoundaryKernel,
                              CL::NullRange,
                              _global,
                              _local);
#ifndef NDEBUG
} catch (CL::Error const& exc) {
  FATAL("An error occured during OpenCL initialization at '%s': %i",
        exc.what(), exc.err());
}

#endif
}

void
LbmSolver::
setBuffers() {
  if (!_swap) {
    _doStreamingKernel.setArg(2, _collideFieldBuffer);
    _doStreamingKernel.setArg(3, _streamFieldBuffer);

    _doCollisionKernel.setArg(2, _streamFieldBuffer);

    _treatBoundaryKernel.setArg(2, _streamFieldBuffer);
  } else {
    _doStreamingKernel.setArg(2, _streamFieldBuffer);
    _doStreamingKernel.setArg(3, _collideFieldBuffer);

    _doCollisionKernel.setArg(2, _collideFieldBuffer);

    _treatBoundaryKernel.setArg(2, _collideFieldBuffer);
  }
}

void
LbmSolver::
swapBuffers() {
  _swap = !_swap;
}

void
LbmSolver::
release() {}
