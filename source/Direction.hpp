#ifndef Direction_hpp
#define Direction_hpp

#include "Handedness.hpp"
#include "Vector.hpp"

namespace Math {
template <class T, Handedness handedness = Handedness::right>
struct Direction {};

template <class T>
struct Direction<T, Handedness::left> {
  using Vector3 = Math::Vector3<T>;

  static Vector3
  left()
  { return -Vector3::UnitX(); }

  static Vector3
  right()
  { return Vector3::UnitX(); }

  static Vector3
  up()
  { return Vector3::UnitY(); }

  static Vector3
  down()
  { return -Vector3::UnitY(); }

  static Vector3
  forward()
  { return Vector3::UnitZ(); }

  static Vector3
  backward()
  { return -Vector3::UnitZ(); }
};

template <class T>
struct Direction<T, Handedness::right> {
  using Vector3 = Math::Vector3<T>;

  static Vector3
  left()
  { return Vector3::UnitX(); }

  static Vector3
  right()
  { return -Vector3::UnitX(); }

  static Vector3
  up()
  { return Vector3::UnitY(); }

  static Vector3
  down()
  { return -Vector3::UnitY(); }

  static Vector3
  forward()
  { return Vector3::UnitZ(); }

  static Vector3
  backward()
  { return -Vector3::UnitZ(); }
};
}

#endif