#ifndef Matrix_hpp
#define Matrix_hpp

#include <Eigen/Geometry>

namespace Math {
template <class T, int height, int width>
using Matrix = Eigen::Matrix<T, height, width>;

template <class T>
using Matrix3x3 = Matrix<T, 3, 3>;

template <class T>
using Matrix4x4 = Matrix<T, 4, 4>;

using Matrix4x4f = Matrix<float, 4, 4>;
}

#endif