#ifndef MovementManager_hpp
#define MovementManager_hpp

#include "Stopwatch.hpp"
#include "Vector.hpp"

class MovementManager {
public:
  MovementManager();

  MovementManager(MovementManager const& other) = delete;

  template <class T>
  Math::Vector3<T> movement() {
    using seconds = boost::chrono::duration<T>;

    auto x = (_leftStopwatch.elapsed<seconds>() -
              _rightStopwatch.elapsed<seconds>()).count();

    auto y = (_upStopwatch.elapsed<seconds>() -
              _downStopwatch.elapsed<seconds>()).count();

    auto z = (_forwardStopwatch.elapsed<seconds>() -
              _backwardStopwatch.elapsed<seconds>()).count();

    reset();

    return Math::Vector3<T>(x, y, z);
  }

  void
  reset();

  void
  startLeft();

  void
  stopLeft();

  void
  startRight();

  void
  stopRight();

  void
  startUp();

  void
  stopUp();

  void
  startDown();

  void
  stopDown();

  void
  startForward();

  void
  stopForward();

  void
  startBackward();

  void
  stopBackward();

private:
  HighResolutionStopwatch _leftStopwatch;
  HighResolutionStopwatch _rightStopwatch;

  HighResolutionStopwatch _upStopwatch;
  HighResolutionStopwatch _downStopwatch;

  HighResolutionStopwatch _forwardStopwatch;
  HighResolutionStopwatch _backwardStopwatch;
};

#endif