#ifndef Printer_hpp
#define Printer_hpp

class QString;

class Printer {
public:
  virtual
  ~Printer() = 0;

  virtual void
  print(QString const& string) = 0;
};

#endif