#ifndef Key_hpp
#define Key_hpp

#include <Qt>

enum class Key: int  {
  a = Qt::Key_A,
  d = Qt::Key_D,
  e = Qt::Key_E,
  q = Qt::Key_Q,
  s = Qt::Key_S,
  w = Qt::Key_W,
};

#endif