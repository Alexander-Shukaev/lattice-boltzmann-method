#include "ParticleRenderer.hpp"

#include "FlagField.hpp"
#include "VectorField.hpp"

#include <oglplus/bound/buffer.hpp>

#include <QtCore/QDebug>

#include <vector>
// -----------------------------------------------------------------------------

using Math::Matrix4x4f;

using oglplus::Buffer;
using oglplus::BufferUsage;
using oglplus::Capability;
using oglplus::DataType;
using oglplus::FragmentShader;
using oglplus::PrimitiveType;
using oglplus::Program;
using oglplus::TransformFeedback;
using oglplus::TransformFeedbackPrimitiveType;
using oglplus::Uniform;
using oglplus::UniformSampler;
using oglplus::VertexArray;
using oglplus::VertexShader;

using std::vector;

ParticleRenderer::
ParticleRenderer(VectorField const* velocityField):
  _velocityField(velocityField),
  _index(1) {
  assert(sampleCountX() >= 1);
  assert(sampleCountY() >= 1);
  assert(sampleCountZ() >= 1);

  _particleCount = sampleCountX() * sampleCountY() * sampleCountZ();

  initializeProgram();
  initializeParticles();
}

GLuint
ParticleRenderer::
sampleCountX() const
{ return _velocityField->sampleCountX(); }

GLuint
ParticleRenderer::
sampleCountY() const
{ return _velocityField->sampleCountY(); }

GLuint
ParticleRenderer::
sampleCountZ() const
{ return _velocityField->sampleCountZ(); }

void
ParticleRenderer::
initializeProgram() {
  VertexShader   vs;
  FragmentShader fs;

  vs.Source(
    "#version 400\n"

    "subroutine void Pass();"
    "subroutine uniform Pass _pass;"

    "uniform sampler3D _velocitySampler;"

    "uniform mat4 _mvp;"

    "uniform uvec3 _texelMaxIndex;"

    "uniform vec3 _positionMax;"

    "uniform float _timeStep;"
    "uniform float _amplifier;"

    "in vec3 _position;"

    "out vec3 v_position;"
    // "out vec3 v_color;"

    "subroutine (Pass)\n"
    "void "
    "update() {"
    "  vec3 velocity  = texture(_velocitySampler, _position / _texelMaxIndex).rgb;"

    "  float timeStep = _amplifier * _timeStep;"

    "  v_position = _position + timeStep * velocity;"
    "  v_position = _position + 0.5 * timeStep *"
    "               (velocity + texture(_velocitySampler, v_position / _texelMaxIndex).rgb);"

    "  v_position = clamp(v_position, vec3(0.5, 0.5, 0.5), _positionMax);"
    "}"

    "subroutine (Pass)\n"
    "void "
    "render() {"
    "  gl_Position = _mvp * vec4(_position, 1);"
    "}"

    "void "
    "main() {"
    "  _pass();"
    "}").Compile();

  fs.Source(
    "#version 400\n"

    // "in vec3 v_color;"

    "out vec4 f_color;"

    "void "
    "main() {"
    "  f_color = vec4(1, 1, 1, 1);"
    "}").Compile();

  _program.AttachShader(vs);
  _program.AttachShader(fs);

  auto          programName = Expose(_program).Name();
  GLchar const* outNames[]  = { "v_position" };

  glTransformFeedbackVaryings(programName, 1, outNames, GL_SEPARATE_ATTRIBS);

  _program.Link();

  _program.DetachShader(vs);
  _program.DetachShader(fs);

  _subroutineUpdate = glGetSubroutineIndex(programName,
                                           GL_VERTEX_SHADER,
                                           "update");
  _subroutineRender = glGetSubroutineIndex(programName,
                                           GL_VERTEX_SHADER,
                                           "render");

  _program.Use();

  UniformSampler(_program, "_velocitySampler").Set(0);

  Uniform<GLuint>(_program, "_texelMaxIndex").SetVector(
    _velocityField->texelCountX() - 1,
    _velocityField->texelCountY() - 1,
    _velocityField->texelCountZ() - 1);

  Uniform<GLfloat>(_program, "_positionMax").SetVector(sampleCountX() + 0.5f,
                                                       sampleCountY() + 0.5f,
                                                       sampleCountZ() + 0.5f);

  Program::UseNone();
}

void
ParticleRenderer::
initializeParticles() {
  _vertexArray[0].Bind();
  {
    auto b = Bind(_positionBuffer[0], Buffer::Target::Array);

    vector<GLfloat> data;

    data.reserve(3 * _particleCount);

    for (int z = 0; z < sampleCountZ(); ++z)
      for (int y = 0; y < sampleCountY(); ++y)
        for (int x = 0; x < sampleCountX(); ++x) {
          data.push_back(x + 1);
          data.push_back(y + 1);
          data.push_back(z + 1);
        }

    b.Data(data, BufferUsage::DynamicDraw);

    (_program | "_position").Setup<GLfloat>(3).Enable();
  }

  _vertexArray[1].Bind();
  {
    auto b = Bind(_positionBuffer[1], Buffer::Target::Array);

    b.Data(3 * _particleCount * sizeof(GLfloat),
           static_cast<void*>(0),
           BufferUsage::DynamicDraw);

    (_program | "_position").Setup<GLfloat>(3).Enable();
  }

  VertexArray::Unbind();

  _transformFeedback[0].Bind();
  _positionBuffer[0].Bind(Buffer::Target::TransformFeedback);
  _positionBuffer[0].BindBase(Buffer::IndexedTarget::TransformFeedback, 0);

  _transformFeedback[1].Bind();
  _positionBuffer[1].Bind(Buffer::Target::TransformFeedback);
  _positionBuffer[1].BindBase(Buffer::IndexedTarget::TransformFeedback, 0);

  TransformFeedback::BindDefault();
}

void
ParticleRenderer::
render(Matrix4x4f const& mvp,
       GLfloat           timeStep,
       GLfloat           amplifier,
       GLfloat           size) const {
  auto depthTest = _gl.IsEnabled(Capability::DepthTest);

  _gl.Enable(Capability::DepthTest);

  _velocityField->attach(0);

  _program.Use();

  Uniform<GLfloat>(_program, "_mvp").SetMatrix<4, 4>(1, mvp.data());
  Uniform<GLfloat>(_program, "_timeStep").Set(timeStep);
  Uniform<GLfloat>(_program, "_amplifier").Set(amplifier);

  // ---------------------------------------------------------------------------

  glUniformSubroutinesuiv(GL_VERTEX_SHADER, 1, &_subroutineUpdate);

  _gl.Enable(Capability::RasterizerDiscard);

  _transformFeedback[_index].Bind();
  TransformFeedback::Begin(TransformFeedbackPrimitiveType::Points);
  {
    _vertexArray[1 - _index].Bind();
    _gl.DrawArrays(PrimitiveType::Points, 0, _particleCount);
  }
  TransformFeedback::End();
  TransformFeedback::BindDefault();

  _gl.Disable(Capability::RasterizerDiscard);

  // ---------------------------------------------------------------------------

  glUniformSubroutinesuiv(GL_VERTEX_SHADER, 1, &_subroutineRender);

  _vertexArray[_index].Bind();
  _gl.PointSize(size);
  _gl.DrawArrays(PrimitiveType::Points, 0, _particleCount);
  _gl.PointSize(1);

  VertexArray::Unbind();

  const_cast<GLuint&>(_index) = 1 - _index;

  // ---------------------------------------------------------------------------

  Program::UseNone();

  _velocityField->detach(0);

  if (!depthTest)
    _gl.Disable(Capability::DepthTest);
}