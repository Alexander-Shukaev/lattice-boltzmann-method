#ifndef Affine_hpp
#define Affine_hpp

#include <Eigen/Geometry>

namespace Math {
template <class T, int size>
using Affine = Eigen::Transform<T, size, Eigen::Affine>;

template <class T>
using Affine3 = Affine<T, 3>;
}

#endif