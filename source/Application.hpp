#ifndef Application_hpp
#define Application_hpp

#include <QtGui/QAction>

#include "MainWindow.hpp"

#include <QtGui/QApplication>

class Animator;
class Console;
class GLWidget;
class Scenario;

class Application: public QApplication {
  Q_OBJECT

public:
  Application(int& argc, char** argv);

private Q_SLOTS:
  void
  initializeScenario(Scenario* scenario);

private:
  Animator*  _animator;
  GLWidget*  _glWidget;
  Console*   _console;
  MainWindow _mainWindow;
};

#endif
