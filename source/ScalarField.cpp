#include "ScalarField.hpp"
// -----------------------------------------------------------------------------

using oglplus::PixelDataFormat;
using oglplus::PixelDataInternalFormat;
using oglplus::PixelDataType;
using oglplus::TextureMagFilter;
using oglplus::TextureMinFilter;

ScalarField::
ScalarField(GLuint sampleCountX,
            GLuint sampleCountY,
            GLuint sampleCountZ): Field(sampleCountX,
                                        sampleCountY,
                                        sampleCountZ,
                                        PixelDataInternalFormat::R32F,
                                        PixelDataFormat::Red,
                                        PixelDataType::Float,
                                        TextureMinFilter::Linear,
                                        TextureMagFilter::Linear) {}
