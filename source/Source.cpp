#include <fstream>

#include "Source.hpp"

#include "LoggingMacros.hpp"
Source::
Source() {
  /* impl */
}

Source::
Source(char const* filePath) {
  // Read source file
  std::ifstream sourceFile(filePath);

  if (!sourceFile.is_open())
    PRINT("Cannot open the file: %s", filePath);

  _source.append(
    std::istreambuf_iterator<char>(sourceFile),
    (std::istreambuf_iterator<char>()));
}

Source::
Source(Source const& other):
  _source(other._source) {}

Source::~Source() {}

Source const&
Source::
operator=(
  Source const& other) {
  _source = other._source;
  return *this;
} /* ----- end of Source::operator= ----- */

::size_t
Source::
length() const {
  return _source.size();
} /* ----- end of Source::length ----- */

char const*
Source::
get() const {
  return _source.c_str();
} /* ----- end of Source::get ----- */
