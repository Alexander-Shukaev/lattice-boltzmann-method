#ifndef KeyboardListener_hpp
#define KeyboardListener_hpp

class KeyboardEvent;

class KeyboardListener {
public:
  virtual
  ~KeyboardListener() = 0;

  virtual void
  press(KeyboardEvent const& event) = 0;

  virtual void
  release(KeyboardEvent const& event) = 0;
};

#endif