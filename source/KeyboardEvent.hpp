#ifndef KeyboardEvent_hpp
#define KeyboardEvent_hpp

#include "Key.hpp"

class KeyboardEvent {
public:
  KeyboardEvent(Key key, bool autoRepeat);

  Key
  key() const;

  bool
  isAutoRepeat() const;

private:
  Key  _key;
  bool _autoRepeat;
};

#endif