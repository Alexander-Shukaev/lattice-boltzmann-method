#ifndef LbmScenario_hpp
#define LbmScenario_hpp

#include "Camera.hpp"
#include "DensityRenderer.hpp"
#include "DomainRenderer.hpp"
#include "EulerRotationManager.hpp"
#include "FlagField.hpp"
#include "IterationCounter.hpp"
#include "MovementManager.hpp"
#include "ParticleRenderer.hpp"
#include "RotationManager.hpp"
#include "ScalarField.hpp"
#include "Scenario.hpp"
#include "VectorField.hpp"
#include "VelocityRenderer.hpp"

#include <GL/glew.h>

#include <oglplus/all.hpp>

#include <QtCore/QObject>

class QGLWidget;

class KeyboardEvent;
class MouseEvent;
class Printer;

class QThread;
class QGLContext;
class LbmWorker;

class LbmScenario: public QObject,
                   public Scenario {
  Q_OBJECT

public:
  LbmScenario(QGLWidget* glWidget);

  void
  initialize();

private:
  void
  initializeRenderers();

public slots:
  void
  reset();

public:
  void
  setPrinter(Printer* printer);

  Printer*
  printer() const;

public slots:
  void
  start();

  void
  toggleSimulationMode();

private slots:
  void
  emitSimulationModeChanged(int mode);

signals:
  void
  simulationModeChanded(int mode);

public slots:
  void
  increaseDelay();

  void
  reduceDelay();

  void
  domainSizeX(int sizeX);

  void
  domainSizeY(int sizeY);

  void
  domainSizeZ(int sizeZ);

  void
  scenarioFilePath(QString scenaioFilePath);

private:
  void
  resize(int width, int height);

  void
  render();

  void
  press(KeyboardEvent const& event);

  void
  release(KeyboardEvent const& event);

  void
  press(MouseEvent const& event);

  void
  release(MouseEvent const& event);

  void
  move(MouseEvent const& event);

  bool     _started;
  Printer* _printer;

  QThread*    _thread;
  QGLContext* _glContext;
  LbmWorker*  _lbmWorker;

  oglplus::Context gl;

  ScalarField* densityField;
  VectorField* velocityField;
  FlagField*   flagField;

  DensityRenderer*  densityRenderer;
  DomainRenderer*   domainRenderer;
  ParticleRenderer* particleRenderer;
  VelocityRenderer* velocityRenderer;

  Cameraf               camera;
  MovementManager       movementManager;
  RotationManagerf      rotationManager;
  EulerRotationManagerf eulerRotationManager;

  IterationCounter _iterationCounter;

  Math::Matrix4x4f projection;
  Math::Matrix4x4f view;

  HighResolutionStopwatch stopwatch;
};

#endif
