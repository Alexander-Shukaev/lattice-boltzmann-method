#include "MovementManager.hpp"
// -----------------------------------------------------------------------------

MovementManager::
MovementManager()
{}

void
MovementManager::
reset() {
  if (_leftStopwatch.isRunning())
    _leftStopwatch.restart();
  else
    _leftStopwatch.reset();

  if (_rightStopwatch.isRunning())
    _rightStopwatch.restart();
  else
    _rightStopwatch.reset();

  if (_upStopwatch.isRunning())
    _upStopwatch.restart();
  else
    _upStopwatch.reset();

  if (_downStopwatch.isRunning())
    _downStopwatch.restart();
  else
    _downStopwatch.reset();

  if (_forwardStopwatch.isRunning())
    _forwardStopwatch.restart();
  else
    _forwardStopwatch.reset();

  if (_backwardStopwatch.isRunning())
    _backwardStopwatch.restart();
  else
    _backwardStopwatch.reset();
}

void
MovementManager::
startLeft()
{ _leftStopwatch.start(); }

void
MovementManager::
stopLeft()
{ _leftStopwatch.stop(); }

void
MovementManager::
startRight()
{ _rightStopwatch.start(); }

void
MovementManager::
stopRight()
{ _rightStopwatch.stop(); }

void
MovementManager::
startUp()
{ _upStopwatch.start(); }

void
MovementManager::
stopUp()
{ _upStopwatch.stop(); }

void
MovementManager::
startDown()
{ _downStopwatch.start(); }

void
MovementManager::
stopDown()
{ _downStopwatch.stop(); }

void
MovementManager::
startForward()
{ _forwardStopwatch.start(); }

void
MovementManager::
stopForward()
{ _forwardStopwatch.stop(); }

void
MovementManager::
startBackward()
{ _backwardStopwatch.start(); }

void
MovementManager::
stopBackward()
{ _backwardStopwatch.stop(); }