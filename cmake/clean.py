#!/usr/bin/env python
# encoding: utf-8

import inspect
import os
import shutil

cmake_dir = os.path.realpath(
    os.path.abspath(
        os.path.split(
            inspect.getfile(
                inspect.currentframe()
            )
        )[0]
    )
)

project_dir = os.path.dirname(cmake_dir)

# Clean {{{
# -----------------------------------------------------------------------------
build_dir = os.path.join(project_dir, 'build')

if os.path.isdir(build_dir):
    shutil.rmtree(build_dir)

install_dir = os.path.join(project_dir, 'install')

if os.path.isdir(install_dir):
    shutil.rmtree(install_dir)
# -----------------------------------------------------------------------------
# }}}
