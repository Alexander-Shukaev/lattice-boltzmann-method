find_path(OpenCL_INCLUDE_DIR
          NAMES CL/cl.h
          PATHS ${OpenCL_INCLUDE_DIR}
                $ENV{OpenCL_INCLUDE_DIR})

find_library(OpenCL_LIBRARY
             NAMES OpenCL
                   OpenCL32
                   OpenCL64
             PATHS ${OpenCL_LIBRARY_DIR}
                   $ENV{OpenCL_LIBRARY_DIR})

set(OpenCL_INCLUDE_DIRS ${OpenCL_INCLUDE_DIR})

set(OpenCL_LIBRARIES ${OpenCL_LIBRARY})

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(OpenCL
                                  DEFAULT_MSG
                                  OpenCL_INCLUDE_DIR
                                  OpenCL_LIBRARY)

mark_as_advanced(OpenCL_INCLUDE_DIR
                 OpenCL_LIBRARY)
