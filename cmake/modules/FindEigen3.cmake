if (NOT Eigen3_FIND_VERSION)
  if (NOT Eigen3_FIND_VERSION_MAJOR)
    set(Eigen3_FIND_VERSION_MAJOR 2)
  endif ()
  if (NOT Eigen3_FIND_VERSION_MINOR)
    set(Eigen3_FIND_VERSION_MINOR 91)
  endif ()
  if (NOT Eigen3_FIND_VERSION_PATCH)
    set(Eigen3_FIND_VERSION_PATCH 0)
  endif ()

  set(Eigen3_FIND_VERSION "${Eigen3_FIND_VERSION_MAJOR}.${Eigen3_FIND_VERSION_MINOR}.${Eigen3_FIND_VERSION_PATCH}")
endif ()

macro(_Eigen3_CHECK_VERSION)
  file(READ "${Eigen3_INCLUDE_DIR}/Eigen/src/Core/util/Macros.h" _Eigen3_VERSION_HEADER)

  string(REGEX MATCH "define[ \t]+EIGEN_WORLD_VERSION[ \t]+([0-9]+)" _Eigen3_WORLD_VERSION_MATCH "${_Eigen3_VERSION_HEADER}")
  set(Eigen3_WORLD_VERSION "${CMAKE_MATCH_1}")

  string(REGEX MATCH "define[ \t]+EIGEN_MAJOR_VERSION[ \t]+([0-9]+)" _Eigen3_MAJOR_VERSION_MATCH "${_Eigen3_VERSION_HEADER}")
  set(Eigen3_MAJOR_VERSION "${CMAKE_MATCH_1}")

  string(REGEX MATCH "define[ \t]+EIGEN_MINOR_VERSION[ \t]+([0-9]+)" _Eigen3_MINOR_VERSION_MATCH "${_Eigen3_VERSION_HEADER}")
  set(Eigen3_MINOR_VERSION "${CMAKE_MATCH_1}")

  set(Eigen3_VERSION ${Eigen3_WORLD_VERSION}.${Eigen3_MAJOR_VERSION}.${Eigen3_MINOR_VERSION})

  if (${Eigen3_VERSION} VERSION_LESS ${Eigen3_FIND_VERSION})
    set(Eigen3_VERSION_OK FALSE)
  else ()
    set(Eigen3_VERSION_OK TRUE)
  endif ()

  if (NOT Eigen3_VERSION_OK)
    message(STATUS "Eigen3 version ${Eigen3_VERSION} found in ${Eigen3_INCLUDE_DIR}, "
                   "but at least version ${Eigen3_FIND_VERSION} is required.")
  endif ()
endmacro()

if (Eigen3_INCLUDE_DIR)
  _Eigen3_CHECK_VERSION()
  set(Eigen3_FOUND ${Eigen3_VERSION_OK})
else (Eigen3_INCLUDE_DIR)
  find_path(Eigen3_INCLUDE_DIR
            NAMES signature_of_eigen3_matrix_library
            PATHS ${Eigen3_INCLUDE_DIR}
                  $ENV{Eigen3_INCLUDE_DIR}
                  ${Eigen_INCLUDE_DIR}
                  $ENV{Eigen_INCLUDE_DIR}
                  ${CMAKE_INSTALL_PREFIX}/include
                  ${KDE4_INCLUDE_DIR}
            PATH_SUFFIXES eigen3
                          eigen)

  if (Eigen3_INCLUDE_DIR)
    _Eigen3_CHECK_VERSION()
  endif ()

  set(Eigen3_INCLUDE_DIRS ${Eigen3_INCLUDE_DIR})

  include(FindPackageHandleStandardArgs)

  find_package_handle_standard_args(Eigen3
                                    DEFAULT_MSG
                                    Eigen3_INCLUDE_DIR
                                    Eigen3_VERSION_OK)

  mark_as_advanced(Eigen3_INCLUDE_DIR)
endif ()
