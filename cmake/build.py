#!/usr/bin/env python
# encoding: utf-8

import inspect
import os
import sys

cmake_dir = os.path.realpath(
    os.path.abspath(
        os.path.split(
            inspect.getfile(
                inspect.currentframe()
            )
        )[0]
    )
)

project_dir = os.path.dirname(cmake_dir)
python_dir = os.path.join(project_dir, 'python')

if python_dir not in sys.path:
    sys.path.insert(0, python_dir)

# Add and parse arguments {{{
# -----------------------------------------------------------------------------
from argparse import ArgumentParser

parser = ArgumentParser()

parser.add_argument('-builder', choices=['ninja', 'make'], default='ninja')
parser.add_argument('-variant', default='Release')
parser.add_argument('-verbose', action='store_true', default=False)

args = parser.parse_args()
# -----------------------------------------------------------------------------
# }}}

# Filter arguments {{{
# -----------------------------------------------------------------------------
args.variant = args.variant.strip()

from error import EmptyArgumentError

if not args.variant:
    raise EmptyArgumentError('VARIANT')
# -----------------------------------------------------------------------------
# }}}

# Prepare Builder arguments {{{
# -----------------------------------------------------------------------------
if args.builder == 'ninja':
    ninja_args = ['ninja']

    if args.verbose:
        ninja_args.append('-v')
elif args.builder == 'make':
    make_args = ['make']

    if args.verbose:
        make_args.append('VERBOSE=1')
# -----------------------------------------------------------------------------
# }}}

# Invoke Builder {{{
# -----------------------------------------------------------------------------
variant_dir = os.path.join(project_dir, 'build', args.variant.lower())

os.chdir(variant_dir)

from process import call

if args.builder == 'ninja':
    call(ninja_args)
elif args.builder == 'make':
    call(make_args)
# -----------------------------------------------------------------------------
# }}}
