def arg(name, value):
    if name is None or value is None:
        return None

    if not value.strip():
        return None

    return '-D' + name + '=' + value
